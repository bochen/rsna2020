'''
Created on Aug 13, 2020

@author: bo
'''

import os 

if 'KAGGLE_ENV' in os.environ:
    KAGGLE_HOME = "/kaggle/input/"
    KAGGLE_INPUT_HOME = os.path.join(KAGGLE_HOME, "rsna-str-pulmonary-embolism-detection")
    KAGGLE_SANDBOX_HOME = "/tmp"
else:
    KAGGLE_HOME = os.path.join(os.environ['HOME'], 'mydev', 'rsna2020')
    KAGGLE_INPUT_HOME = os.path.join(KAGGLE_HOME, 'input')
    KAGGLE_SANDBOX_HOME = os.path.join(KAGGLE_HOME, 'sandbox')

study_level_weights = {
    'negative_exam_for_pe': 0.0736196319,
    'indeterminate':    0.09202453988,
    'chronic_pe': 0.1042944785,
    'acute_and_chronic_pe': 0.1042944785,
    'central_pe': 0.1877300613,
    'leftsided_pe': 0.06257668712,
    'rightsided_pe': 0.06257668712,
    'rv_lv_ratio_gte_1': 0.2346625767,
    'rv_lv_ratio_lt_1': 0.0782208589,
}

image_level_weight = 0.07361963

STUDY_COLUMNS = list(study_level_weights.keys())
IMAGE_COLUMN = 'pe_present_on_image'
ALL_COLUMNS = [IMAGE_COLUMN] + STUDY_COLUMNS

IMAGE_SIZES = (224, 224)
