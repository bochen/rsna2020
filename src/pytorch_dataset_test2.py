'''
Created on Oct 3, 2020

@author: ec2-user
'''
import unittest
from pytorch_dataset import RNNDataset, CNNDataset, Erode, Rescale, RandomCrop
import numpy as np 
import matplotlib.pyplot as plt


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def show(self, seq, title="None"):
        images = seq['image'][:10] + 0.5
        i = 1
        plt.figure(figsize=(9, len(images) * 3))
        for arr in images:
            img = np.transpose(arr[:3], (1, 2, 0))
            mask1 = arr[3]
            mask2 = arr[4]
            plt.subplot(len(images), 3, i)
            plt.imshow(img)
            plt.axis('off')
            plt.subplot(len(images), 3, i + 1)
            plt.imshow(mask1, cmap='gray')
            plt.axis('off')
            plt.subplot(len(images), 3, i + 2)
            plt.imshow(mask2, cmap='gray')
            plt.axis('off')
            i += 3
        
        plt.suptitle(title)
        plt.show()        

    def testDilate(self):
        studyid = 'cdc4ce03759e'
        ds = RNNDataset([studyid], transform=Erode())
        print(len(ds))
        self.show(ds[0], "testDilate")

    def testRescale(self):
        studyid = 'cdc4ce03759e'
        ds = RNNDataset([studyid], transform=Rescale(output_size=(256, 256)))
        print(len(ds))
        assert ds[0]['image'][0][0].shape == (256, 256)
        self.show(ds[0], 'testRescale')

    def testRandomCrop(self):
        studyid = 'cdc4ce03759e'
        ds = RNNDataset([studyid], transform=RandomCrop(output_size=(200, 200)), to_tensor=True)
        print(len(ds))
        assert ds[0]['image'][0][0].shape == (200, 200)
        self.show(ds[0], 'testRandomCrop')

        
if __name__ == "__main__":
    unittest.main()
