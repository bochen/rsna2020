'''
Created on Oct 3, 2020

@author: ec2-user
'''

import torchvision.models as models
from torch import nn
from config import STUDY_COLUMNS
import torch
import config
from pytorch_dataset import option
from efficientnet_pytorch import EfficientNet

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Identity(nn.Module):

    def __init__(self):
        super(Identity, self).__init__()
        
    def forward(self, x):
        return x
    

class StudyLevelModule(nn.Module):

    def __init__(self, seq_len, num_features, dr_rate, rnn_hidden_size, rnn_num_layers):
        super(StudyLevelModule, self).__init__()
        self.rnn_type = 'LSTM'
        self.rnn_hidden_size = rnn_hidden_size 
        self.rnn_num_layers = rnn_num_layers
        n_output = len(STUDY_COLUMNS)
        self.seq_len = seq_len
        dropouts = [nn.Dropout(dr_rate) for _ in range(n_output)]
        self.rnn = nn.LSTM(num_features, rnn_hidden_size, rnn_num_layers, bidirectional=option.bidirectional)
        n_hidden_dim = rnn_hidden_size
        if option.bidirectional:
            n_hidden_dim *= 2
        if option.rnn_max_and_average:
            n_hidden_dim *= 2
        outputs = [nn.Linear(n_hidden_dim, 1) for _ in range(n_output)]
        for i, u in enumerate(dropouts):
            setattr(self, "dropout_" + str(i), u) 
        for i, u in enumerate(outputs):
            setattr(self, "output_" + str(i), u) 

    def get_dropout(self, i):
            return getattr(self, "dropout_" + str(i)) 

    def get_output(self, i):
            return getattr(self, "output_" + str(i)) 

    def forward(self, ys):
        ts = self.seq_len
        ii = 0
        y = ys[ii]
        self.rnn.flatten_parameters()
        if option.rnn_average:
            outs = []
            out, (hn, cn) = self.rnn(y.unsqueeze(1))
            outs.append(out)
            for ii in range(1, ts):
                y = ys[ii]
                out, (hn, cn) = self.rnn(y.unsqueeze(1), (hn, cn))
                outs.append(out)
            outs = torch.stack(outs)
            out = torch.mean(outs, dim=0)
            out = out[:, -1]
        elif option.rnn_max:
            outs = []
            out, (hn, cn) = self.rnn(y.unsqueeze(1))
            outs.append(out)
            for ii in range(1, ts):
                y = ys[ii]
                out, (hn, cn) = self.rnn(y.unsqueeze(1), (hn, cn))
                outs.append(out)
            outs = torch.stack(outs)
            out = torch.max(outs, dim=0)[0]
            out = out[:, -1] 
            
        elif option.rnn_max_and_average:
            outs = []
            out, (hn, cn) = self.rnn(y.unsqueeze(1))
            outs.append(out)
            for ii in range(1, ts):
                y = ys[ii]
                out, (hn, cn) = self.rnn(y.unsqueeze(1), (hn, cn))
                outs.append(out)
            outs = torch.stack(outs)
            out1 = torch.mean(outs, dim=0)[:, -1]
            out2 = torch.max(outs, dim=0)[0][:, -1]
            out = torch.cat([out1, out2], dim=1)
        elif option.rnn_last:
            out, (hn, cn) = self.rnn(y.unsqueeze(1))
            for ii in range(1, ts):
                y = ys[ii]
                out, (hn, cn) = self.rnn(y.unsqueeze(1), (hn, cn))
            out = out[:, -1]
            
        ret = {}
        for i, c in enumerate(STUDY_COLUMNS):
            x = self.get_dropout(i)(out)
            x = self.get_output(i)(x)
            ret[c] = x 
        return ret        
    
    
class ImageLevelModule(nn.Module):

    def __init__(self, num_features, dr_rate):
        super(ImageLevelModule, self).__init__()
        self.dropout = nn.Dropout(dr_rate)
        self.pe_present_on_image = nn.Sequential(nn.Linear(num_features, 1))

    def forward(self, x):
        x = self.dropout(x)
        x = self.pe_present_on_image(x) 
        return x        
    
    
class MyExtentNet(nn.Module):

    def __init__(self, my_pretrained_model, num_features, dr_rate=0.5, rnn_hidden_size=128, rnn_num_layers=1, as_cnn=False):
        
        super(MyExtentNet, self).__init__()
        self.pretrained = my_pretrained_model
        self.dr_rate = dr_rate
        self.image_level = ImageLevelModule(num_features, dr_rate)
        self.study_level = StudyLevelModule(option.seqlen(), num_features, dr_rate, rnn_hidden_size, rnn_num_layers)
        self.as_cnn = as_cnn

    def forward(self, xd):
        x = xd['image']
        if option.use_pretrained:
            x = x[:, :, :3]
        if self.as_cnn:
            x = self.pretrained(x)
            x1 = self.image_level(x)
            return x1
        else:
            b_z, ts, c, h, w = x.shape
            ys = []
            image_outputs = []
            for i in range(ts):
                y = self.pretrained((x[:, i]))
                ys.append(y)
                image_outputs.append(self.image_level(y))

            study_outputs = self.study_level(ys)
            return image_outputs, study_outputs


class TotalLoss(object):

    def __init__(self):
        self.n_study_target = torch.tensor(len(STUDY_COLUMNS), dtype=torch.float32, device=device)
        self.criterion = torch.nn.BCEWithLogitsLoss(reduction='none')
        self.torch_study_level_weights = {k:torch.tensor(v, dtype=torch.float32, device=device) 
                                          for k, v in config.study_level_weights.items()}
        self.torch_image_level_weight = torch.tensor(config.image_level_weight,
                                                  dtype=torch.float32, device=device) 
        self.one = torch.tensor(1, dtype=torch.float32, device=device)
        self.two = torch.tensor(2, dtype=torch.float32, device=device)
        self.eps = torch.tensor(1e-7, dtype=torch.float32, device=device)

    def __call__(self, inputs, outputs):    
        image_outputs, study_outputs = outputs
        image_targets = inputs['image_target']
        study_targets = inputs['study_target']
        study_pos_frac = inputs['study_pos_frac'].unsqueeze(1)
        img_seq_frac = inputs['img_seq_frac'].unsqueeze(1)
        study_masks = inputs['study_target_mask'].unsqueeze(1)
        _, ts = image_targets.shape
        study_loss = torch.tensor(0, dtype=torch.float32, device=device)
        image_loss = torch.tensor(0, dtype=torch.float32, device=device)
        
        if 1:
            for k, v in study_targets.items():  # study level
                tmp = self.criterion(study_outputs[k], v.unsqueeze(1)) * self.torch_study_level_weights[k]
                study_loss = study_loss + torch.sum(tmp * study_masks) / (torch.sum(study_masks) + self.eps)
            no_study_level = torch.sum(study_masks) < self.eps
            for i in range(ts):  # imagelevel
                tmp = self.criterion(image_outputs[i], image_targets[:, i].unsqueeze(1))
                image_loss = image_loss + torch.sum(tmp * study_pos_frac) 
            image_loss = image_loss / (torch.sum(study_pos_frac) * torch.tensor(ts, dtype=torch.float32, device=device))
            if no_study_level:
                return  image_loss, study_loss, image_loss
            else:
                return  (study_loss + image_loss) / self.two, study_loss, image_loss, no_study_level
        else:
            for k, v in study_targets.items():  # study level
                tmp = self.criterion(study_outputs[k], v.unsqueeze(1)) * self.torch_study_level_weights[k]
                study_loss = study_loss + torch.sum(tmp * study_masks * img_seq_frac)
            for i in range(ts):  # imagelevel
                tmp = self.criterion(image_outputs[i], image_targets[:, i].unsqueeze(1))
                image_loss = image_loss + torch.sum(tmp * study_pos_frac) * self.torch_image_level_weight 
                
            sum_weights = torch.sum(study_masks * img_seq_frac) + \
                        torch.sum(study_pos_frac) * torch.tensor(ts, dtype=torch.float32, device=device) * self.torch_image_level_weight  #
            if sum_weights < self.one:
                sum_weights = self.one 
            return  (study_loss + image_loss) / sum_weights, study_loss / sum_weights, image_loss / sum_weights 


total_loss = TotalLoss()


def make_model(name, dr_rate=0.5, rnn_hidden_size=128, rnn_num_layers=1):
    my_pretrained_model = None 
    if name.startswith('resnet'):
        f = getattr(models, name)
        my_pretrained_model = f(pretrained=option.use_pretrained)
        if not option.use_pretrained:
            my_pretrained_model.conv1 = torch.nn.Conv2d(5, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        num_features = my_pretrained_model.fc.in_features
        my_pretrained_model.fc = Identity()
    elif name.startswith('densenet'):  # densenet161
        f = getattr(models, name)
        my_pretrained_model = f(pretrained=option.use_pretrained)
        # my_pretrained_model.features.conv0.in_channels = 5
        if not option.use_pretrained:
            my_pretrained_model.features.conv0 = torch.nn.Conv2d(5, 96, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        num_features = my_pretrained_model.classifier.in_features
        my_pretrained_model.classifier = Identity()
    elif name.startswith('vgg'):  # vgg16
        f = getattr(models, name)
        my_pretrained_model = f(pretrained=option.use_pretrained)
        if not option.use_pretrained:
            my_pretrained_model.features[0] = torch.nn.Conv2d(5, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        num_features = my_pretrained_model.classifier[6].in_features
        my_pretrained_model.classifier[6] = Identity()
    elif name.startswith('inception'):
        f = getattr(models, name)
        my_pretrained_model = f(pretrained=option.use_pretrained, aux_logits=False)
        if not option.use_pretrained:
            my_pretrained_model.Conv2d_1a_3x3 = torch.nn.Conv2d(5, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)
        num_features = my_pretrained_model.fc.in_features
        my_pretrained_model.fc = Identity()

    elif name.startswith('efficientnet'):
        if option.use_pretrained:
            my_pretrained_model = EfficientNet.from_name(name, in_channels=5)
        else:
            my_pretrained_model = EfficientNet.from_pretrained(name)
        num_features = my_pretrained_model._fc.in_features
        my_pretrained_model._fc = Identity()
        
    assert my_pretrained_model is not None 
    
    my_extended_model = MyExtentNet(my_pretrained_model=my_pretrained_model, num_features=num_features
                                    , dr_rate=dr_rate, rnn_hidden_size=rnn_hidden_size, rnn_num_layers=rnn_num_layers)
    return my_extended_model


if __name__ == '__main__':
    pass
