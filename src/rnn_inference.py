'''
Created on Oct 9, 2020

@author: ec2-user
'''
import option
import os
import torch
from pytorch_train import build_model, to_device
import data
from pytorch_dataset import RNNDataset
import numpy as np
import pandas as pd  
from tqdm import tqdm 
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def load_model(path):
    model = build_model()[0]
    model = torch.nn.DataParallel(model)
    assert os.path.exists(path)
    checkpoint = torch.load(path, map_location=device)
    model.load_state_dict(checkpoint['model_state_dict'])
    return model


def get_train_study_list(study_list=None):
    if study_list is None: 
        return data.get_train_study_list()
    else:
        return study_list


def get_test_study_list(study_list=None, ignore_private=False):
    if study_list is None: 
        return data.get_test_study_list(ignore_private=ignore_private)
    else:
        return study_list


def make_dataloader(study_list, batch_size=1, is_train=False):
    ds = RNNDataset(study_list, is_train=is_train, transform=None, full_seq=True, return_ids=True)    
    # dl = torch.utils.data.DataLoader(ds, batch_size=batch_size, num_workers=1)
    return ds


def sigmoid(x):
  return 1 / (1 + np.exp(-x))

          
def predict(model, dataset):
    model.eval()
    image_ret = {}
    study_ret = {}
    with torch.no_grad():
        t = tqdm(dataset)
        t.set_description('predict')
        for batch in t:
            img = batch['image']
            img = np.expand_dims(img, 0)
            ids = batch['ids']
            studyid = ids[0].split('/')[0]
            batch = torch.tensor(img, dtype=torch.float32, device=device)
            image_outputs, study_outputs = model({'image':batch})
            image_outputs = np.array([u[0][0].item() for u in image_outputs])
            study_outputs = {k:v[0][0].item() for k, v in study_outputs.items()}
            study_ret[studyid] = study_outputs
            for id, p in zip(ids, image_outputs):
                image_ret[id] = p
    a, b = pd.Series(image_ret), pd.DataFrame(study_ret)
    a = a.map(sigmoid)
    b = b.apply(sigmoid).T
    return a, b

            
def test_inference(model):
    pass


if __name__ == '__main__':
    modelpath = "../input/model/resnet50/latest_model.pt"

    model = load_model(modelpath)
    if 0:  # train
        is_train = True 
        study_list = ["0003b3d648eb", "000f7f114264", "00102474a2db", "0038fd5f09f5", "0045f113e031"][:3]
        dl = make_dataloader(study_list, is_train=is_train)
        pred1, pred2 = predict(model, dl)
        print(pred1, "\n", pred2)
    else:
        is_train = False
        study_list = ["00268ff88746", "00c53115a9fa", "00e7015490cb"][:3]
        dl = make_dataloader(study_list, is_train=is_train)
        pred1, pred2 = predict(model, dl)
        print(pred1, "\n", pred2)

