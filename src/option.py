'''
Created on Oct 4, 2020

@author: ec2-user
'''
import config
import os

learning_rate = 0.001
init_weight_decay = 1e-08
momentum_value = 0.9

epochs = 200
batch_size = 16 
save_dir = "snapshot"
img_dir = os.path.join(config.KAGGLE_INPUT_HOME, 'img')
resume = False
aug = True  # model
use_model2 = True 

use_pretrained=False 
# select optim algorhtim to train
Adam = True
SGD = False
Adadelta = False
bidirectional = True
init_clip_max_norm = 3
rnn_hidden_dim = 256
rnn_num_layers = 1
dr_rate = 0.5
num_threads = 4
seed_num = 123

half_window_size = 10  # #of slices of rnn = 2*half_window_size+1
percent_top_slices = 0.3  # predict study/exam level use percent_top_slices of slices as window center 
max_dilate_iterations = 10

valid_ratio = 0.2

rnn_method = "max_and_average"
rnn_average = False
rnn_last = False 
rnn_max = False 
rnn_max_and_average = True

# base_model = 'efficientnet-b0'
base_model = 'resnet50'


def seqlen():
    return half_window_size * 2 + 1


def show_options():
    for k, v in globals().items():
        if not k.startswith('__'):
            print("option: {}={}".format(str(k), str(v)))


def update_rnn_method():
    global rnn_average
    global rnn_last
    global rnn_max
    global rnn_max_and_average
    rnn_average = False
    rnn_last = False 
    rnn_max = False 
    rnn_max_and_average = False
    
    if rnn_method in ['average', 'mean']:
        rnn_average = True
    elif rnn_method == 'max':
        rnn_max = True
    elif rnn_method == 'last':
        rnn_last = True
    elif rnn_method in ['max_and_average', 'average_and_max', 'avg_and_max', 'max_and_avg']:
        rnn_max_and_average = True
    
    assert rnn_max_and_average or rnn_max or rnn_last or rnn_average
    
