'''
Created on Aug 16, 2020

@author: bo
'''

import pickle 
import gzip
import os, sys


def pickle_dumpz(obj, filepath):
    with gzip.open(filepath, 'w', compresslevel=1) as f:
        pickle.dump(obj, f)

        
def pickle_loadz(filepath):
    with gzip.open(filepath, 'r') as f:
        return pickle.load(f)        


def create_folder_if_not_exists(path):
    if not os.path.exists(path):
        os.mkdir(path)
