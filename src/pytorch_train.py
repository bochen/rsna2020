
import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import argparse
import random
import numpy as np 
import torch
import torch.optim.lr_scheduler as lr_scheduler
from tqdm import tqdm 

import option 
from pytorch_dataset import Rescale, RNNDataset, make_rnn_train_valid_dataloader, \
    default_transforms
from utils import create_folder_if_not_exists
from torchvision import transforms

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def make_transform():
    input_size = (299, 299) if option.base_model.startswith('inception') else None
    transform = None
    if option.aug:
        transform = None 
        if input_size is not None:
            transform = transforms.Compose(default_transforms + [Rescale(output_size=input_size)])
        else:
            transform = transforms.Compose(default_transforms)         
    else:
        if input_size is not None:
            transform = Rescale(output_size=input_size)
    return transform


def make_dataset():
    transform = make_transform()
    return make_rnn_train_valid_dataloader(transform=transform, valid_ratio=option.valid_ratio,
                                           batch_size=option.batch_size, num_workers=option.num_threads)


def build_model():
    global make_model
    model = make_model(option.base_model, dr_rate=option.dr_rate, rnn_hidden_size=option.rnn_hidden_dim, rnn_num_layers=option.rnn_num_layers)
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        model = torch.nn.DataParallel(model)
    elif device == 'cpu':
        print("Let's use CPU!")
    
    model.to(device)
    # print(model)

    if option.Adam is True:
        print("Adam Training......")
        optimizer = torch.optim.Adam(model.parameters(), lr=option.learning_rate, weight_decay=option.init_weight_decay)
    elif option.SGD is True:
        print("SGD Training.......")
        optimizer = torch.optim.SGD(model.parameters(), lr=option.learning_rate, weight_decay=option.init_weight_decay,
                                    momentum=option.momentum_value)
    elif option.Adadelta is True:
        print("Adadelta Training.......")
        optimizer = torch.optim.Adadelta(model.parameters(), lr=option.learning_rate, weight_decay=option.init_weight_decay)

    scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, 'min', verbose=True)
    return model, optimizer, scheduler


def to_device(d, device):
    return {k:to_device(v, device) if isinstance(v, dict) else v.type(torch.float32).to(device) for k, v in d.items()}


def eval_valid(epoch, model, optimizer, scheduler, valid_ds):
    global total_loss
    model.eval()
    step = 0
    sum_losses = [0, 0, 0]
    with torch.no_grad():
        t = tqdm(valid_ds)
        t.set_description('valid')
        for batch in t:

            inputs = to_device(batch, device)
            image_outputs, study_outputs = model(inputs)
            triple_losses = total_loss(inputs, (image_outputs, study_outputs))
            for i in range(3):
                sum_losses[i] += triple_losses[i].detach().cpu().numpy()
            step += 1
            t.set_postfix(total_loss=sum_losses[0] / step, study_loss=sum_losses[1] / step, img_loss=sum_losses[2] / step)
    losses = sum_losses[0] / step, sum_losses[1] / step, sum_losses[2] / step
    print("\nepoch {}: valid total loss={:.6f}, study_loss={:.6f}, image_loss={:.6f}".format(epoch, *losses), flush=True)
    try_save_model(epoch, model, optimizer, scheduler, losses, best=True)    
    return losses[0]


_best_loss = np.inf


def try_save_model(epoch, model, optimizer, scheduler, losses, best=False):
    global _best_loss
    if (not best) or (best and losses[0] < _best_loss):
        create_folder_if_not_exists(option.save_dir)
        path = os.path.join(option.save_dir, '{}_model.pt'.format('best' if best else 'latest'))
        if best:
            _best_loss = losses[0]
            print("saving best model (losses={:.6f}) to {}".format(_best_loss, path))
        torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'scheduler_state_dict': scheduler.state_dict(),
                'loss': losses,
                'best_loss':_best_loss
                }, path)    


def load_model(best=False):
    path = os.path.join(option.save_dir, '{}_model.pt'.format('best' if best else 'latest'))
    assert os.path.exists(path)
    checkpoint = torch.load(path)
    return checkpoint


def train_epoch(epoch, train_ds, model, optimizer, scheduler):
    model.train()
    if 1: 
        sum_losses = [0, 0, 0]
        step = 0
        t = tqdm(train_ds)
        t.set_description('train')
        for batch in t:
            if 1:
                
                inputs = to_device(batch, device)
    
                optimizer.zero_grad()
                model.zero_grad()
    
                image_outputs, study_outputs = model(inputs)
                triple_losses = total_loss(inputs, (image_outputs, study_outputs))
                for i in range(3):
                    sum_losses[i] += triple_losses[i].detach().cpu().numpy()
                triple_losses[0].backward()
                if option.init_clip_max_norm > 0:
                    torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=option.init_clip_max_norm)
                optimizer.step()
                step += 1
                t.set_postfix(total_loss=sum_losses[0] / step, study_loss=sum_losses[1] / step, img_loss=sum_losses[2] / step)                

        losses = sum_losses[0] / step, sum_losses[1] / step, sum_losses[2] / step
        try_save_model(epoch, model, optimizer, scheduler, losses, best=False)
        print("\nepoch {}: train total loss={:.6f}, study_loss={:.6f}, image_loss={:.6f}".format(epoch, *losses), flush=True)


def train():
    train_ds, valid_ds = make_dataset()
    start_epoch = 1
    model, optimizer, scheduler = build_model()
    
    if option.resume:
        checkpoint = load_model(best=False)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        if 'scheduler_state_dict' in checkpoint:
            scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
        start_epoch = checkpoint['epoch'] + 1
        global _best_loss
        _best_loss = checkpoint['best_loss']
    
    for epoch in  (range(start_epoch, option.epochs + 1)):
        train_epoch(epoch, train_ds, model, optimizer, scheduler)
        if valid_ds is not None:
            val_loss = eval_valid(epoch, model, optimizer, scheduler, valid_ds)
            scheduler.step(val_loss)


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)
 
    if 1:
        # Setup argument parser
        parser = ArgumentParser(description='train', formatter_class=RawDescriptionHelpFormatter)
        
        parser = argparse.ArgumentParser(description="text classification")
        # learning
        parser.add_argument('-lr', type=float, default=option.learning_rate, help='initial learning rate ')
        parser.add_argument('-epochs', type=int, default=option.epochs, help='number of epochs for train ')
        parser.add_argument('-batch_size', type=int, default=option.batch_size, help='batch size for training ')
        parser.add_argument('-save_dir', type=str, default=option.save_dir, help='where to save the snapshot')
        parser.add_argument('-img_dir', type=str, default=option.img_dir, help='where train data is saved')
        parser.add_argument('-base_model', type=str, default=option.base_model, help='base_model')
        parser.add_argument('-rnn_method', type=str, default=option.rnn_method, help='rnn method')
        
        # optim select
        parser.add_argument('-Adam', action='store_true', default=option.Adam, help='whether to select Adam to train')
        parser.add_argument('-SGD', action='store_true', default=option.SGD, help='whether to select SGD to train')
        parser.add_argument('-Adadelta', action='store_true', default=option.Adadelta, help='whether to select Adadelta to train')
        
        parser.add_argument('-bidirectional', action='store_true', default=option.bidirectional, help='bidirectional lstm')
        parser.add_argument('-resume', action='store_true', default=option.resume, help='resume training')
        parser.add_argument('-aug', action='store_true', default=option.aug, help='aug training')
        parser.add_argument('-use_model2', action='store_true', default=option.use_model2, help='use models')
        parser.add_argument('-use_pretrained', action='store_true', default=option.use_pretrained, help='load pretrained weights')

        # 
        parser.add_argument('-half_window_size', type=int, default=option.half_window_size, help='half_window_size')
        parser.add_argument('-max_dilate_iterations', type=int, default=option.max_dilate_iterations, help='max_dilate_iterations')
        parser.add_argument('-seed_num', type=int, default=option.seed_num, help='seed_num')                        
        parser.add_argument('-percent_top_slices', type=float, default=option.percent_top_slices, help='percent_top_slices')
        parser.add_argument('-valid_ratio', type=float, default=option.valid_ratio, help='valid_ratio')

        parser.add_argument('-num_threads', type=int, default=option.num_threads, help='num_threads')

        args = parser.parse_args()
        for k, v in vars(args).items():
            setattr(option, k, v)
        
        option.update_rnn_method()
        
        option.show_options()
    
    global make_model, total_loss
    if option.use_model2:
        import pytorch_model2
        make_model = pytorch_model2.make_model
        total_loss = pytorch_model2.total_loss
    else:
        import pytorch_model
        make_model = pytorch_model.make_model
        total_loss = pytorch_model.total_loss

    torch.manual_seed(option.seed_num)
    random.seed(option.seed_num)
    np.random.seed(option.seed_num)
    
    train()
    
    return 0


if __name__ == "__main__":
    # args = '-batch-size 4 -epochs 2'.split()
    args = []
    sys.exit(main(args))
