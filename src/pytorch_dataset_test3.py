'''
Created on Oct 3, 2020

@author: ec2-user
'''
import unittest
from pytorch_dataset import RNNDataset
import numpy as np 
import matplotlib.pyplot as plt
import torch


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDataLoader(self):
        studyid = ['cdc4ce03759e'] * 10
        ds = RNNDataset(studyid, transform=None)
        dataset_loader = torch.utils.data.DataLoader(ds,
                                             batch_size=4, shuffle=True,
                                             num_workers=4)
        
        for x in dataset_loader:
            for k, v in x.items():
                if k in ['study_target_mask', 'study_target']:
                    print(k, v)
                else:
                    print (k, v.shape, v.dtype)
            break
        
        
if __name__ == "__main__":
    unittest.main()
