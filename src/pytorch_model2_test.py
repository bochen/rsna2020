'''
Created on Oct 3, 2020

@author: ec2-user
'''
from pytorch_dataset import RNNDataset, Rescale, default_transforms
import numpy as np 
import torch
from pytorch_model2 import make_model, total_loss
import option
from torchvision import transforms
        
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
option.use_pretrained = True 


def test_model(modelname, input_size=None):
    model = make_model(modelname)
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        model = torch.nn.DataParallel(model)
    else:
        print("Let's use CPU!")
    
    model.to(device)
    model.train()
    # print(model)
    
    studyid = ['cdc4ce03759e'] * 10
    transform = None 
    if input_size is not None:
        transform = transforms.Compose(default_transforms + [Rescale(output_size=input_size)])
    else:
        transform = transforms.Compose(default_transforms)
    ds = RNNDataset(studyid, transform=transform)
    dataset_loader = torch.utils.data.DataLoader(ds,
                                         batch_size=option.batch_size,  # shuffle=True,
                                         num_workers=4)

    def to_device(d, device):
        return {k:to_device(v, device) if isinstance(v, dict) else v.type(torch.float32).to(device) for k, v in d.items()}
    
    for data in dataset_loader:
        if 1:
            input = to_device(data, device)
            image_outputs, study_outputs = model(input)
            print("length", len(image_outputs), len(study_outputs))
            print("input")
            for k, v in input.items():
                if k == 'image':
                    print(k, v.shape, v.dtype) 
                else:
                    print(k, v) 
            print("image_output")
            for x in image_outputs:
                print(x.shape, x.dtype)
            print("study_output")
            for k, v in study_outputs.items():
                print(k, v.shape, v.dtype) 
            
            loss = total_loss(input, (image_outputs, study_outputs))
            
            print("loss", loss[0].dtype, loss)
            # import time;time.sleep(111111)
            break
        break


if __name__ == "__main__":
    option.bidirectional = True
    option.batch_size = 4
    option.rnn_method = 'max_and_avg'
    option.update_rnn_method()
    option.show_options()
    
    # test_model('resnet18')
    test_model('vgg11')
    # test_model('resnet50')
    # test_model('densenet161')
    # test_model('inception_v3', input_size=(299, 299))

    # test_model('efficientnet-b0')
    
