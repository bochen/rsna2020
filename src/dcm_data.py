'''
Created on Oct 1, 2020

@author: ec2-user
'''

import pydicom
import  tarfile
import io
import os
import numpy as np
from tqdm import tqdm
import cv2
 
from config import KAGGLE_INPUT_HOME
from utils import pickle_dumpz, create_folder_if_not_exists
import pickle 
import gzip
from scipy.signal import savgol_filter
import pandas as pd 


def _get_folder(is_train):
    return  os.path.join(KAGGLE_INPUT_HOME, 'train' if is_train else 'test')

    
def get_studies(is_train):
    return os.listdir(_get_folder(is_train))


def read_series(rootpath, studyid, seriesid):
    folder = os.path.join(rootpath, studyid, seriesid)
    sop_ids = [u for u in os.listdir(folder) if u.endswith('.dcm')]
    slices = []
    for sop in sop_ids:
        with open(os.path.join(folder, sop), 'rb') as f:
            img = pydicom.dcmread(f)
            t = (studyid, seriesid, sop)
            slices.append((t, img))
    
    slices = sorted(slices, key=lambda u: int(u[1].ImagePositionPatient[2]))
                              
    return slices    

 
def read_study(rootpath, studyid):
    slices = []
    folder = os.path.join(rootpath, studyid)
    seriesids = os.listdir(folder)
    for seriesid in seriesids:
        slices .append(read_series(rootpath, studyid, seriesid))
    return slices


def dicom_to_img(dcm):
    raw_pixelarrays = dcm.pixel_array
    raw_pixelarrays[raw_pixelarrays <= -1000] = 0
    img = raw_pixelarrays * dcm.RescaleSlope + dcm.RescaleIntercept
    return img


def thresholding(img, width, level):
    hw = width // 2
    newimg = img.copy()
    low = level - hw
    high = level + hw
    newimg[img > high] = high
    newimg[img < low] = low
    newimg = (newimg - low) / (width) * 255
    return newimg.astype(np.uint8)


def refine_input(im_th):
    cnts, hir = cv2.findContours(im_th, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in cnts] 
    if len(areas) == 0: return im_th
    i = np.argsort(areas)[-1]
    cnt = cnts[i]
    a = np.zeros([*im_th.shape, 3], dtype=np.uint8)
    cv2.drawContours(a, cnts, i, (255, 255, 255), thickness=cv2.FILLED);
    mask = (a[:, :, 0] > 0).astype(np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.dilate(mask, kernel, iterations=20)    
    return (mask > 0) * im_th


def refine_mask(mask):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.dilate(mask, kernel, iterations=20)

    cnts, hir = cv2.findContours(255 - mask * 255, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in cnts] 
    if len(areas) == 0: return mask
    # if len(areas)==0: return mask*0
    i = np.argsort(areas)[-1]
    cnt = cnts[i]
    a = np.zeros([*mask.shape, 3], dtype=np.uint8)
    cv2.drawContours(a, cnts, i, (255, 255, 255), thickness=cv2.FILLED);
    return a[:, :, 0]


def flushing(im_th, b_show=False):
    im_th = refine_input(im_th)
    im_floodfill = im_th.astype(np.uint8).copy() 
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255);
    cv2.floodFill(im_floodfill, mask, (w - 1, 0), 255);
    cv2.floodFill(im_floodfill, mask, (0, h - 1), 255);
    cv2.floodFill(im_floodfill, mask, (w - 1, h - 1), 255);
    mask = refine_mask(mask)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    mask = mask[1:-1, 1:-1]
    im_out = mask * im_floodfill_inv
    if b_show:
        import matplotlib.pyplot as plt
        plt.subplot(2, 2, 1)
        plt.imshow(im_th, cmap='gray')  
        plt.axis('off')
        plt.title("orignal")
        plt.subplot(2, 2, 2)
        plt.axis('off')
        plt.imshow(mask, cmap='gray')    
        plt.title("mask")
        plt.subplot(2, 2, 3)
        plt.axis('off')
        plt.imshow(im_floodfill, cmap='gray')    
        plt.title("floodfill")
        plt.subplot(2, 2, 4)
        plt.axis('off')
        plt.imshow(im_out, cmap='gray')    
        plt.title("out")
        plt.show()
    return im_out, mask


def th(img, low, high):
    a = img.copy() 
    a[a < low] = low
    a[a > high] = high

    a = (a - low) / (a.max() - a.min() + 1e-7)

    im_th1 = ((a > 0.5) * 255).astype(np.uint8)
    return im_th1


def th3(img, threshold):
    a = img.copy() 
    a[img > threshold] = 1
    a[img <= threshold] = 0
    im_th1 = (a).astype(np.uint8)
    return im_th1


def erode(img):
    kernel = np.ones((5, 5), np.uint8)
    if 1:
        dilation = cv2.erode(img, kernel, iterations=10)
        return dilation
    else:
        opening = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=30)
        return opening


def dilate(img):
    kernel = np.ones((5, 5), np.uint8)
    if 1:
        dilation = cv2.dilate(img, kernel, iterations=10)
        return dilation
    else:
        opening = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=30)
        return opening


def closing(img):
    kernel = np.ones((5, 20), np.uint8)    
    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=3)
    Contours, Hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    img2 = img * 0
    for cnt in Contours:
        hull = cv2.convexHull(cnt)
        cv2.drawContours(img2, [hull], -1, (255, 255, 255), -1) 
    
    return (img2 > 0).astype(np.uint8)
    
    
def phase4_make_lungmask(img, b_show=False):
    img = preprocess0(img)
    im_th1 = th(img, -600, -600 + 1)
    _, mask = flushing(im_th1, b_show)
    img = img * (mask > 0)
    im_th2 = 1 - th3(img, -500)
    return (mask > 0).astype(np.uint8), im_th2


def preprocess0(im_th):
    a = [im_th[0, 0], im_th[0, -1], im_th[-1, 0], im_th[-1, -1]]

    def cornner(a):
        d = {}
        for u in a:
            d[u] = 0
        for u in a:
            d[u] += 1
        s = sorted(list(d.items()), key=lambda u:-u[1])
        return s[0][0]

    a = cornner(a)
    im_th = im_th.copy()
    im_th[im_th == a] = -1000
    return im_th

 
def to_rgb(img, use_mask=True):
    if use_mask:
        mask1, mask2 = phase4_make_lungmask(img)
    else:
        mask1 = np.ones_like(img, dtype=np.uint8)
        mask2 = np.ones_like(img, dtype=np.uint8)
    a = thresholding(img, width=1000, level=-750) * mask1
    b = thresholding(img, width=400, level=40) * mask1
    c = thresholding(img, width=700, level=100) * mask1
    return np.array([a, b, c]).transpose([1, 2, 0]), mask1, mask2


def to_cnn_img(img, mask1, mask2):
    return cv2.resize(img, (224, 224)), cv2.resize(mask1, (224, 224)), cv2.resize(mask2, (224, 224))


class Slice:

    @staticmethod
    def empty():
        return Slice('', '', '',
                     np.zeros([224, 224, 3], dtype=np.float32),
                     np.zeros([224, 224], dtype=np.float32),
                     np.zeros([224, 224], dtype=np.float32), _lung_area=0)
    
    def __init__(self, studyid, seriesid, sopid, img, mask1 , mask2, _lung_area=None):
        self.studyid = studyid
        self.seriesid = seriesid
        self.sopid = sopid.replace('.dcm', '')
        self.img = img
        self.mask1 = mask1
        self.mask2 = mask2
        self._lung_area = _lung_area
    
    def copy(self):
        return Slice(self.studyid, self.seriesid, self.sopid, self.img.copy(),
                     self.mask1.copy(), self.mask2.copy(), self.lung_area())
        
    def lung_area(self):
        if self._lung_area is None:
            self._lung_area = np.sum(self.mask2)
            if self._lung_area > 8399 * 2: 
                self._lung_area = np.nan
        return self._lung_area

    @property
    def id(self):
        return "{}/{}/{}".format(self.studyid, self.seriesid, self.sopid)
    
    @property
    def id2(self):
        return "{}_{}_{}".format(self.studyid, self.seriesid, self.sopid)
     
    def plot(self):
        import matplotlib.pyplot as plt
        plt.subplot(1, 3, 1)
        plt.imshow(self.img)
        plt.subplot(1, 3, 2)
        plt.imshow(self.mask1)
        plt.subplot(1, 3, 3)
        plt.imshow(self.mask2)
        plt.suptitle(self.id())
        plt.show()

        
class Slices:

    def __init__(self):
        self.slices = []
        self._z = None  
        self._lung_area = None 

    def get_studyid(self):
        return self.slices[0].studyid

    def add(self, s:Slice):
        self.slices.append(s)
    
    def size(self):
        return len(self.slices)
    
    def get(self, i)->Slice:
        return self.slices[i]
    
    @property
    def z(self):
        if self._z is None:
            self._z = np.array([i / self.size() for i in range(self.size())], dtype=np.float32)
        return self._z

    @property
    def lung_area(self):
        if self._lung_area is None:
            _lung_area = np.array([x.lung_area() for x in self.slices])
            s = pd.Series(_lung_area)
            s = s.fillna(method='ffill').fillna(method='bfill')
            self._lung_area = s.values.astype(np.float32) / s.max()
            self._lung_area = smooth_data_np_convolve(self._lung_area)
        return self._lung_area

    def plot_lung_area(self):
        import matplotlib.pyplot as plt
        plt.plot(self.z, self.lung_area)
        plt.show()

            
def slices_to_rgb(slices) -> Slices:
    res = Slices()
    for k, dcm in slices:
        img = dicom_to_img(dcm)
        img, mask1, mask2 = to_cnn_img(*to_rgb(img, True))
        res.add(Slice(*k, img, mask1, mask2))
    return res


def load_study(is_train, studyid, b_show=False):
    rootpath = _get_folder(is_train)
    slices = read_study(rootpath, studyid)
    assert len(slices) == 1
    slices = slices[0]
    a = slices_to_rgb(slices)
    if b_show:
        a.get(a.size() // 2).plot()
        a.plot_lung_area()
    return a 

    
def process_study(is_train, studyid, outdir, skip_exists=True, b_show=False):
    outfile = os.path.join(outdir, studyid) + '.pklz'
    if skip_exists and os.path.exists(outdir):
        print("skip " + outfile)
    else:
        a = load_study(is_train, studyid, b_show=b_show)
        pickle_dumpz(a, outfile)


def smooth_savgol_filter(x):
    return  savgol_filter(x, 51, 2)


def smooth_data_np_convolve(arr):
    span = len(arr) // 5
    if span > 51: span = 30
    if span < 1: span = 1
    return np.convolve(arr, np.ones(span * 2 + 1) / (span * 2 + 1), mode="same")


if __name__ == '__main__':
    studies = get_studies(True)
    print("#train studies: " + str(len(studies)))
    outdir = "/tmp/train"
    create_folder_if_not_exists(outdir)
    for studyid in studies:
        process_study(True, studyid, outdir, skip_exists=False, b_show=True)
    
    studies = get_studies(False)
    print("#test studies: " + str(len(studies)))
    outdir = "/tmp/test"
    create_folder_if_not_exists(outdir) 
    for studyid in studies:
        process_study(False, studyid, outdir, skip_exists=False, b_show=True)   

