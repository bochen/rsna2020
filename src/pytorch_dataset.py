'''
Created on Oct 2, 2020

@author: ec2-user
'''

import os
import torch
import pandas as pd
import numpy as np
from torch.utils.data import Dataset, DataLoader, IterableDataset
from dcm_data import load_study, Slices, Slice
from make_train_z_info import load_train_data, make_z_info
from data import get_test_study_list, get_train_study_list
from utils import pickle_loadz
import config
from config import IMAGE_COLUMN, STUDY_COLUMNS
import cv2
import option
from torch.utils.data._utils.pin_memory import pin_memory
from torchvision import transforms


class ZInfo:

    def __init__(self, d):
        sorted_lung_area_index = d['sorted_lung_area_index']
        k = max(1, int(option.percent_top_slices * len(sorted_lung_area_index)))
        self.top_index = set(sorted_lung_area_index[:k])

        
def _make_target(df):
    df['key'] = df['StudyInstanceUID,SeriesInstanceUID,SOPInstanceUID'.split(',')].apply(lambda u: "/".join(u), axis=1)
    image_target = df[['key', IMAGE_COLUMN]].set_index('key')[IMAGE_COLUMN].to_dict()
    study_target = df[["StudyInstanceUID"] + STUDY_COLUMNS].drop_duplicates().set_index('StudyInstanceUID').T.to_dict()
    study_pos_frac = df[["StudyInstanceUID", IMAGE_COLUMN]].groupby('StudyInstanceUID').mean().iloc[:, 0]
    study_pos_frac[study_pos_frac < 1.0 / 300] = 1.0 / 300 
    study_pos_frac = study_pos_frac.to_dict()
    return image_target, study_target, study_pos_frac


class RNNDataset(IterableDataset):

    def __init__(self, study_list, transform=None, is_train=True, return_ids=False, full_seq=False):
        self.study_list = study_list
        self.transform = transform
        self.is_train = is_train
        self.return_ids = return_ids
        self.full_seq = full_seq
        self._z_info = {}
        if self.is_train:
            d = pickle_loadz(os.path.join(config.KAGGLE_INPUT_HOME, "train_z_info.pklz"))
            self._z_info = {k:ZInfo(d[k]) for k in study_list}
        
        self.target = None, None 
        if self.is_train:
            file = os.path.join(config.KAGGLE_INPUT_HOME, 'train.csv')
            if not os.path.exists(file):
                file = os.path.join(config.KAGGLE_INPUT_HOME, 'train.csv.gz')
            df = pd.read_csv(file)
            self.target = _make_target(df[df['StudyInstanceUID'].isin(set(self.study_list))])
    
    def study_target(self, studyid):
        return self.target[1][studyid]

    def  study_pos_frac(self, studyid):
        return self.target[2][studyid]

    def image_target(self, imageid):
        if self.is_train:
            return self.target[0][imageid]
        else:
            return 0
    
    def get_z_info(self, slices:Slices):
        id = slices.get_studyid()
        if id not in self._z_info:
            a = make_z_info(slices)
            self._z_info[id] = ZInfo(a) 
        return self._z_info[id]
    
    def get_seq_bk(self, sample:Slices, center_idx):
        if self.full_seq:
            end_idx = sample.size() - 1
            seq = []
            idx = 0
            study_target_mask = 1

        else:
            end_idx = center_idx + option.half_window_size
            seq = []
            idx = center_idx - option.half_window_size
            study_target_mask = 1 if (center_idx in self.get_z_info(sample).top_index) else 0
        
        while idx <= end_idx:
            if(idx < 0 or idx >= sample.size()):
                s = Slice.empty()
                if self.transform is not None:
                    s = self.transform(s)                   
                t = 0
            else:
                s = sample.get(idx)
                if self.transform is not None:
                    s = self.transform(s)   
                t = self.image_target(s.id)
            img = np.transpose(s.img / 255., (2, 0, 1)).astype(np.float32)
            img = [img, np.expand_dims(s.mask1.astype(np.float32), 0), np.expand_dims(s.mask2.astype(np.float32), 0)]
            img = np.concatenate(img, axis=0)
            seq.append([img, t, s.id])
            idx += 1
        
        image_target = np.array([u[1] for u in seq], dtype=np.float32)
        # for u in seq: print(u[0].shape)
        image = np.array([u[0] for u in seq], dtype=np.float32) - 0.5
        ids = np.array([u[2] for u in seq])
                    
        if self.is_train:
            ret = { 'study_target_mask':study_target_mask,
                   'study_pos_frac': self.study_pos_frac(sample.get_studyid()),
                   'img_seq_frac': len(image_target) / sample.size(),
                    'study_target': {k:float(v) for k, v in self.study_target(sample.get_studyid()).items()},
                "image_target":image_target,
                'image':image, }
        else:
            ret = { 'image':image, }
        
        if self.return_ids:
            ret['ids'] = ids
        return ret  
    
    def get_seq(self, sample:Slices, center_idx):
        if self.full_seq:
            seq = []
            study_target_mask = 1
            index = list(range(sample.size()))
        else:
            index = list(range(sample.size()))
            np.random.shuffle(index)
            seqlen = option.seqlen()
            index = index[:seqlen]
            while len(index) < seqlen:
                index.append(-1)
            index = sorted(index)
                
            seq = []
            study_target_mask = 1
        
        for idx in index:
            if(idx < 0 or idx >= sample.size()):
                s = Slice.empty()
                if self.transform is not None:
                    s = self.transform(s)                   
                t = 0
            else:
                s = sample.get(idx)
                if self.transform is not None:
                    s = self.transform(s)   
                t = self.image_target(s.id)
            img = np.transpose(s.img / 255., (2, 0, 1)).astype(np.float32)
            img = [img, np.expand_dims(s.mask1.astype(np.float32), 0), np.expand_dims(s.mask2.astype(np.float32), 0)]
            img = np.concatenate(img, axis=0)
            seq.append([img, t, s.id])
        
        image_target = np.array([u[1] for u in seq], dtype=np.float32)
        # for u in seq: print(u[0].shape)
        image = np.array([u[0] for u in seq], dtype=np.float32) - 0.5
        ids = np.array([u[2] for u in seq])
                    
        if self.is_train:
            ret = { 'study_target_mask':study_target_mask,
                   'study_pos_frac': self.study_pos_frac(sample.get_studyid()),
                   'img_seq_frac': len(image_target) / sample.size(),
                    'study_target': {k:float(v) for k, v in self.study_target(sample.get_studyid()).items()},
                "image_target":image_target,
                'image':image, }
        else:
            ret = { 'image':image, }
        
        if self.return_ids:
            ret['ids'] = ids
        return ret  
        
    def __iter__(self):
        np.random.shuffle(self.study_list)
        for studyid in self.study_list:
            if self.is_train:
                sample:Slices = load_train_data(studyid, os.path.join(option.img_dir, 'train'))
            else:
                sample:Slices = load_train_data(studyid, os.path.join(option.img_dir, 'test'))
                # sample:Slices = load_study(is_train=False, studyid=studyid, b_show=False)
        
            n = sample.size()
            if self.full_seq:
                yield self.get_seq(sample, 0)
            else:
                i = int(np.random.random() * n)
                yield self.get_seq(sample, i)
                

class CNNDataset(IterableDataset):

    def __init__(self, study_list, transform=None, is_train=True, center_zero=True, public_test=False, full_study=True):
        self.study_list = study_list
        self.transform = transform
        self.is_train = is_train
        self.full_study = full_study
        self.center_zero = center_zero
        self.public_test = public_test
        self.target = None 
        if self.is_train:
            file = os.path.join(config.KAGGLE_INPUT_HOME, 'train.csv')
            if not os.path.exists(file):
                file = os.path.join(config.KAGGLE_INPUT_HOME, 'train.csv.gz')
            df = pd.read_csv(file)
            self.target = _make_target(df[df['StudyInstanceUID'].isin(set(self.study_list))])[0]
    
    def image_target(self, imageid):
        return self.target[imageid]
    
    def get_image(self, sample:Slices, idx):
        s = sample.get(idx)
        if self.transform is not None:
            s = self.transform(s)   
        img = np.transpose(s.img / 255., (2, 0, 1)).astype(np.float32)
        img = [img, np.expand_dims(s.mask1.astype(np.float32), 0), np.expand_dims(s.mask2.astype(np.float32), 0)]
        image = np.concatenate(img, axis=0) 
        if self.center_zero: image -= 0.5
        if self.is_train:
            image_target = self.image_target(s.id)
        if self.is_train:
            return {"id":s.id,
                "image_target":float(image_target),
                'image':image, }
        else:
            return {"id":s.id,
                'image':image, }
    
    def __iter__(self):
        np.random.shuffle(self.study_list)
        for studyid in self.study_list:
            if self.is_train:
                sample:Slices = load_train_data(studyid, os.path.join(option.img_dir, 'train'))
            else:
                if self.public_test:
                    sample:Slices = load_train_data(studyid, os.path.join(option.img_dir, 'test'))
                else:
                    sample:Slices = load_study(is_train=False, studyid=studyid, b_show=False)
        
            n = sample.size()
            if 0:
                i = int(np.random.random() * n)
                yield self.get_image(sample, i)
            else:
                if self.full_study:
                    r = {}
                    for i in range(n):
                        a = self.get_image(sample, i)
                        for k, v in a.items(): 
                            if k not in r: r[k] = []
                            r[k].append(v)
                    yield {k:np.array(v) for k, v in r.items()}
                else:
                    for i in np.random.permutation(range(n)):
                        yield self.get_image(sample, i)


class Rescale(object):

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        ret = sample.copy()
        image = np.concatenate([ret.img, np.expand_dims(ret.mask1, -1), np.expand_dims(ret.mask2, -1)], axis=-1)
        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

        image = cv2.resize(image, (new_w, new_h))
        
        ret.img = image[:, :, :3].astype(np.uint8)
        ret.mask1 = (image[:, :, 3] > 0.5).astype(np.uint8)
        ret.mask2 = (image[:, :, 4] > 0.5).astype(np.uint8)

        return ret 


class RandomCrop(object):

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        ret = sample.copy()
        image = np.concatenate([ret.img, np.expand_dims(ret.mask1, -1), np.expand_dims(ret.mask2, -1)], axis=-1)
        h, w = image.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)
        
        ret.img = ret.img[top: top + new_h,
                      left: left + new_w]
        ret.mask1 = ret.mask1[top: top + new_h,
                      left: left + new_w]
        ret.mask2 = ret.mask2[top: top + new_h,
                      left: left + new_w]
        return ret

    
class Erode(object):

    def __init__(self, n_iteration=None):
        self.kernel = np.ones((5, 5), np.uint8)
        self.n_iteration = n_iteration 

    def dilate(self, mask):
        if self.n_iteration is None:
            iterations = int(np.random.random() * option.max_dilate_iterations)
        else:
            iterations = self.n_iteration
            
        dilation = cv2.erode(mask, self.kernel, iterations=iterations)
        return dilation

    def __call__(self, sample:Slice) -> Slice :
        ret = sample.copy()
        mask1 = (self.dilate(ret.mask1) > 0).astype(np.uint8)
        ret.mask1 = mask1
        ret.mask2 = (mask1 * ret.mask2).astype(np.uint8)
        ret.img = (np.expand_dims(mask1, axis=-1) * ret.img).astype(np.uint8)
        
        return ret


default_transforms = [Erode(), Rescale(output_size=(256, 256)), RandomCrop(output_size=(224, 224))]    


def make_rnn_train_valid_dataset(transform=None, valid_ratio=0.2, random_seed=None):
    if random_seed is not None: 
        np.random.seed(random_seed)
    study_list = get_train_study_list()
    np.random.shuffle(study_list)
    if valid_ratio > 0:
        k = int(len(study_list) * valid_ratio)
        valid_list = study_list[:k]
        train_list = study_list[k:]
        print("#train={}, #valid={}".format(len(train_list), len(valid_list)))
        return RNNDataset(train_list, transform=transform, is_train=True), \
            RNNDataset(valid_list, transform=None, is_train=True)
    else:
        train_list = study_list
        print("no valid data. #train={}".format(len(train_list)))
        return RNNDataset(train_list, transform=transform, is_train=True), None
    

def make_rnn_train_valid_dataloader(transform=None, valid_ratio=0.2, random_seed=None, batch_size=32, num_workers=4):
    trainds, testds = make_rnn_train_valid_dataset(transform, valid_ratio, random_seed)
    dl1 = torch.utils.data.DataLoader(trainds,
                                             batch_size=batch_size,  # shuffle=True,
                                             num_workers=num_workers)
    if testds is None:
        dl2 = None
    else:
        dl2 = torch.utils.data.DataLoader(testds,
                                             batch_size=batch_size,  # shuffle=False,
                                             num_workers=num_workers)    
    return dl1, dl2


def make_rnn_test_dataset(transform=None, ignore_private=False, full_seq=True):
    return RNNDataset(get_test_study_list(ignore_private=ignore_private), transform=transform, full_seq=full_seq)

    
def make_cnn_test_dataset(transform=None, ignore_private=False):
    return CNNDataset(get_test_study_list(ignore_private=ignore_private), transform=transform)


def make_vae_dataset(transform=None):
    a = CNNDataset(get_test_study_list(ignore_private=False), transform=transform, center_zero=False, public_test=True, is_train=False)
    b = CNNDataset(get_train_study_list(), transform=transform, center_zero=False)
    return VAEDataset([a, b])


class VAEDataset(IterableDataset):

    def __init__(self, dataset_list):
        self.dataset_list = dataset_list

    def __iter__(self):
        for dataset in  self.dataset_list:
            for a in dataset: 
                yield a


def make_VAE_dataloader(transform=None, batch_size=32, num_workers=4, pin_memory=False):
    trainds = make_vae_dataset(transform=transform)
    dl1 = torch.utils.data.DataLoader(trainds,
                                             batch_size=batch_size,  # shuffle=True,
                                             num_workers=num_workers, pin_memory=pin_memory)
    return dl1 


def make_cnn_train_valid_dataset(transform=None, valid_ratio=0.2, random_seed=None, center_zero=True):
    if random_seed is not None: 
        np.random.seed(random_seed)
    study_list = get_train_study_list()
    np.random.shuffle(study_list)
    if valid_ratio > 0:
        k = int(len(study_list) * valid_ratio)
        valid_list = study_list[:k]
        train_list = study_list[k:]
        print("#train={}, #valid={}".format(len(train_list), len(valid_list)))
        return CNNDataset(train_list, transform=transform, is_train=True, center_zero=center_zero), \
            CNNDataset(valid_list, transform=None, is_train=True, center_zero=center_zero)
    else:
        train_list = study_list
        print("no valid data. #train={}".format(len(train_list)))
        return CNNDataset(train_list, transform=transform, is_train=True, center_zero=center_zero), None
    

def make_cnn_train_valid_dataloader(transform=None, valid_ratio=0.2, random_seed=None, batch_size=32, num_workers=4, pin_memory=False, center_zero=True):
    trainds, testds = make_cnn_train_valid_dataset(transform, valid_ratio, random_seed, center_zero=center_zero)
    dl1 = torch.utils.data.DataLoader(trainds,
                                             batch_size=batch_size,  # shuffle=True,
                                             num_workers=num_workers, pin_memory=pin_memory)
    if testds is None:
        dl2 = None
    else:
        dl2 = torch.utils.data.DataLoader(testds,
                                             batch_size=batch_size,  # shuffle=False,
                                             num_workers=num_workers, pin_memory=pin_memory)    
    return dl1, dl2


if __name__ == '__main__':
    pass
