'''
Created on Oct 2, 2020

@author: ec2-user

since train pklz data were made by notebook. checked if it is consistent with python code
'''


import os, sys
import config
import utils
from dcm_data import Slices, Slice, load_study
import numpy as np 
from make_train_z_info import load_train_data
import unittest



class TestStringMethods(unittest.TestCase):
    def _compare(self,a:Slice,b:Slice):
        self.assertEquals(a.id(),b.id())
        self.assertTrue(np.allclose(a.img,b.img))
        self.assertTrue(np.allclose(a.mask1,b.mask1))
        self.assertTrue(np.allclose(a.mask2,b.mask2))
    def test_slices(self):
        studyid = 'cdc4ce03759e'
        s1 = load_study(True, studyid)
        s2=load_train_data(studyid)
        self.assertEquals(s1.size(), s2.size())
        self.assertTrue(np.allclose(s1.z,s2.z))
        self.assertTrue(np.allclose(s1.lung_area,s2.lung_area))
        for a,b in zip(s1.slices,s2.slices):
            self._compare(a,b)
    
if __name__ == '__main__':
    unittest.main()
