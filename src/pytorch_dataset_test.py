'''
Created on Oct 3, 2020

@author: ec2-user
'''
import unittest
from pytorch_dataset import RNNDataset, CNNDataset
import numpy as np 


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testRNNDataset(self):
        studyid = 'cdc4ce03759e'
        ds = RNNDataset([studyid])
        print(len(ds))
        a = ds[0]
        for k, v in a.items():
            if k in ['study_target_mask', 'study_target', 'image_target']:
                print(k, v)
            else:
                print (k, v.shape, v.dtype)
        
        for i in range(5):
            print(a['image'][10][i].max(), a['image'][10][i].min())

    def testCNNDataset(self):
        studyid = 'cdc4ce03759e'
        ds = CNNDataset([studyid])
        a = ds[0]
        print(len(ds), len(a))
        
        for d in a[:3]:
            for k, v in d.items():
                if k in [ 'id', 'image_target']:
                    print(k, v)
                elif k == 'image':
                    print(k, v.shape, v.dtype, v.max(), v.min())
                else:
                    print (k, v.shape, v.dtype)

    def test_compare_CNNDataset_RNNDataset(self):
        studyid = 'cdc4ce03759e'
        ds = CNNDataset([studyid])
        a = ds[0]

        ds = RNNDataset([studyid])
        print(len(ds))
        b = ds[0]
        images = b['image']
        ids = b['ids']
        for i, img in zip(ids, images):
            b_found = False
            for d in a:
                if d['id'] == i:
                    b_found = True
                    assert np.allclose(img, d['image'])
            assert b_found
            
        
if __name__ == "__main__":
    unittest.main()
