'''
Created on Oct 9, 2020

@author: ec2-user
'''
import option
import os
import torch
import data
import numpy as np
import pandas as pd  
from tqdm import tqdm 
import pytorch_ResNetVAE_train as vae
from pytorch_dataset import CNNDataset
import utils
from config import KAGGLE_INPUT_HOME
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def load_model(path):
    model = vae.build_model(only_encoder=True)
    model = torch.nn.DataParallel(model)
    assert os.path.exists(path)
    checkpoint = vae.load_model(path=path)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.to(device)
    return model


def get_train_study_list(study_list=None):
    if study_list is None: 
        return data.get_train_study_list()
    else:
        return study_list


def get_test_study_list(study_list=None, ignore_private=False):
    if study_list is None: 
        return data.get_test_study_list(ignore_private=ignore_private)
    else:
        return study_list


def make_dataloader(study_list, batch_size=1, is_train=False):
    ds = CNNDataset(study_list, transform=None, center_zero=False, public_test=True, is_train=is_train, full_study=True)
    return ds

          
def predict(model, dataset):
    model.eval()
    z_ret = {}
    with torch.no_grad():
        t = tqdm(dataset)
        t.set_description('predict')
        for batch in t:
            img = batch['image']
            # img = np.expand_dims(img, 0)
            ids = batch['id']
            studyid = ids[0].split('/')[0]
            batch = torch.tensor(img, dtype=torch.float32, device=device)
            z = model(batch).cpu().numpy()
            z_ret[studyid] = [ids, z]
    return z_ret

            
def run_debug():
    modelpath = "../input/model/results_vae/latest_model.pt"

    model = load_model(modelpath)
    if 0:  # train
        is_train = True 
        study_list = ["0003b3d648eb", "000f7f114264", "00102474a2db", "0038fd5f09f5", "0045f113e031"][:3]
        dl = make_dataloader(study_list, is_train=is_train)
        z_ret = predict(model, dl)
        for k, v in z_ret.items():
            print(k, v)
            break
    else:
        is_train = False
        study_list = ["00268ff88746", "00c53115a9fa", "00e7015490cb"][:3]
        dl = make_dataloader(study_list, is_train=is_train)
        z_ret = predict(model, dl)
        for k, v in z_ret.items():
            print(k, v)
            break


def run_train(model, outpath):
    if 1:  # train
        is_train = True 
        study_list = data.get_train_study_list()
        dl = make_dataloader(study_list, is_train=is_train)
        z_ret = predict(model, dl)
    utils.pickle_dumpz(z_ret, outpath)

    
def run_test(model, outpath, ignore_private=False):
    if 1:  # train
        is_train = False
        study_list = data.get_test_study_list(ignore_private)
        dl = make_dataloader(study_list, is_train=is_train)
        z_ret = predict(model, dl)
        utils.pickle_dumpz(z_ret, outpath)

                
if __name__ == '__main__':
    option.img_dir='/tmp'
    modelpath = os.path.join(KAGGLE_INPUT_HOME, "model", "results_vae", "latest_model.pt")
    outpath = os.path.join(KAGGLE_INPUT_HOME, "test_vae_z.pklz")
    model = load_model(modelpath)
    
    run_test(model, outpath)
    #run_train(model, outpath)
