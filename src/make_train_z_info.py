'''
Created on Oct 2, 2020

@author: ec2-user
'''
import os, sys
import config
import utils
from dcm_data import Slices, Slice
from tqdm import tqdm 
import numpy as np 
TRIAN_IMG_FOLDER = os.path.join(config.KAGGLE_INPUT_HOME, 'img', 'train')


def load_train_data(studyid, img_folder=None):
    if img_folder is None:
        img_folder = TRIAN_IMG_FOLDER
    lst = utils.pickle_loadz(os.path.join(img_folder, '{}.pklz'.format(studyid)))
    s = Slices()
    
    for k, img, mask1, mask2 in lst:
        s.add(Slice(*k, img, mask1, mask2))
    return s


def make_z_info(slices):
    sorted_lung_area_index = np.argsort(slices.lung_area)[::-1]
    return {'z': slices.z, 'lung_area':slices.lung_area, 'sorted_lung_area_index':sorted_lung_area_index }

    
if __name__ == '__main__':
    result = {}
    files = os.listdir(TRIAN_IMG_FOLDER)
    for file in tqdm(files): 
        studyid = file.split('.')[0]
        slices:Slices = load_train_data(studyid)
        result[studyid] = make_z_info(slices)
        if 0:
            import matplotlib.pyplot as plt
            plt.scatter(slices.z, slices.lung_area)
            plt.show()
        
    out = os.path.join(config.KAGGLE_INPUT_HOME, 'train_z_info.pklz')
    utils.pickle_dumpz(result, out)
