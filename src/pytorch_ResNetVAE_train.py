import os
import glob
import numpy as np
import torch
import torch.nn.functional as F
import pickle
from pytorch_dataset import make_cnn_train_valid_dataloader, default_transforms, \
    make_VAE_dataloader
from utils import create_folder_if_not_exists
import sys
from modules import ResNet_VAE
from torchvision import transforms
from tqdm import tqdm 
import option
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# EncoderCNN architecture
CNN_fc_hidden1, CNN_fc_hidden2 = 1024, 1024
CNN_embed_dim = 256  # latent dim extracted by 2D CNN
res_size = 224  # ResNet image size
dropout_p = 0.2  # dropout probability

use_aug = True
# training parameters
epochs = 20  # training epochs
batch_size = 128
log_interval = 10  # interval for displaying training info

# save model
save_model_path = './results_vae'

_best_loss = 99999


def try_save_model(epoch, model, optimizer, losses, best=False):
    global _best_loss
    if (not best) or (best and losses[0] < _best_loss):
        create_folder_if_not_exists(save_model_path)
        path = os.path.join(save_model_path, '{}_model.pt'.format('best' if best else 'latest'))
        if best:
            _best_loss = losses[0]
            print("saving best model (losses={:.6f}) to {}".format(_best_loss, path))
        torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': losses,
                'best_loss':_best_loss
                }, path)    


def load_model(path=None, best=False):
    if path is None:
        path = os.path.join(save_model_path, '{}_model.pt'.format('best' if best else 'latest'))
    assert os.path.exists(path)
    checkpoint = torch.load(path, map_location=device)
    return checkpoint


def check_mkdir(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)


bce_criterion = torch.nn.BCEWithLogitsLoss(reduction='sum')  # MSE = F.mse_loss(recon_x, x, reduction='sum')


def loss_function(recon_x, x, mu, logvar):
    
    b_z = x.shape[0]
    MSE = F.binary_cross_entropy(recon_x, x, reduction='sum') / b_z
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp()) / b_z
    return MSE + KLD, MSE, KLD


def train(log_interval, model, device, train_loader, optimizer, epoch):
    # set model as training mode
    model.train()

    total_loss = 0
    total_mse = 0 
    total_kld = 0
    total_bce = 0
    N_count = 0  # counting total trained sample in one epoch
    step = 0
    t = tqdm(enumerate(train_loader))
    t.set_description('train')
    for batch_idx, batch in t:
        X = batch["image"]
        X = X.to(device)
        N_count += X.size(0)

        optimizer.zero_grad()
        X_reconst, z, mu, logvar = model(X)  # VAE
        loss, mse, kld = loss_function(X_reconst, X, mu, logvar)
        total_loss += (loss.item())
        total_mse += mse.item() 
        total_kld += kld.item() 
        # total_bce += bce.item()

        loss.backward()
        optimizer.step()
        step += 1
        d = dict(total_loss=total_loss / step, mse=total_mse / step, kld=total_kld / step, bce=total_bce / step)
        t.set_postfix({k:float_to_str(v) for k, v in d.items()})
        if step % 5000 == 0:
            try_save_model(epoch, model, optimizer, loss, best=False)
        # if (batch_idx + 1) % log_interval == 0:
    try_save_model(epoch, model, optimizer, loss, best=False)
    print('Train Epoch: {}, step: {} \tLoss: {:.6f} mse: {:.6f} kld: {:.6f} bce: {:.6f}'.format(
        epoch + 1, step, total_loss / step, total_mse / step, total_kld / step, total_bce / step), flush=True)


def make_transform():
    transform = None
    if use_aug:
        transform = transforms.Compose(default_transforms)         
    return transform


def float_to_str(x):
    return '{:.4f}'.format(x)


def validation(model, device, optimizer, test_loader, epoch):
    # set model as testing mode
    model.eval()

    total_loss = 0
    total_mse = 0 
    total_kld = 0
    total_bce = 0
    step = 0
    with torch.no_grad():
        t = tqdm(enumerate(test_loader))
        t.set_description('valid')
        for _, batch in t:
            X = batch["image"]
            y = batch["image_target"]            
            # distribute data to device
            X, y = X.to(device), y.to(device).view(-1,)
            X_reconst, z, mu, logvar, pe_present_on_image_logits = model(X)

            loss, mse, kld, bce = loss_function(X_reconst, X, mu, logvar, y, pe_present_on_image_logits)
            total_loss += (loss.item())
            total_mse += mse.item()  
            total_kld += kld.item() 
            total_bce += bce.item()
            step += 1
            d = dict(total_loss=total_loss / step, mse=total_mse / step, kld=total_kld / step, bce=total_bce / step)
            t.set_postfix({k:float_to_str(v) for k, v in d.items()})

        print('Valid Epoch: {}, step: {} \tLoss: {:.6f} mse: {:.6f} kld: {:.6f} bce: {:.6f}'.format(
                epoch + 1, step, total_loss / step, total_mse / step, total_kld / step, total_bce / step))


def build_model(only_encoder=False):
    return ResNet_VAE(fc_hidden1=CNN_fc_hidden1, fc_hidden2=CNN_fc_hidden2, drop_p=dropout_p, CNN_embed_dim=CNN_embed_dim, only_encoder=only_encoder)


def main(resume=False):
    use_cuda = torch.cuda.is_available()  # check if GPU exists
    device = torch.device("cuda" if use_cuda else "cpu")  # use CPU or GPU
    
    option.img_dir = '/tmp/img'
    option.show_options()

    # Data loading parameters
    transform = make_transform() 
    
    if 0:
        train_loader, valid_loader = make_cnn_train_valid_dataloader(transform=transform,
                                                                 batch_size=batch_size, num_workers=4,
                                                                 pin_memory=True,
                                                                 center_zero=False)
    
    train_loader = make_VAE_dataloader(transform=transform, batch_size=batch_size, num_workers=4,
                                                                 pin_memory=True)
    # Create model
    resnet_vae = build_model()
    model_params = list(resnet_vae.parameters())
    optimizer = torch.optim.Adam(model_params)
    start_epoch = 0
    if resume:
        checkpoint = load_model(best=False)
        resnet_vae.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        start_epoch = checkpoint['epoch'] + 1
        global _best_loss
        _best_loss = checkpoint['best_loss']
    
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        resnet_vae = torch.nn.DataParallel(resnet_vae)
    elif device == 'cpu':
        print("Let's use CPU!")
    resnet_vae.to(device)
    
    print("Using", torch.cuda.device_count(), "GPU!")
    
    create_folder_if_not_exists(save_model_path)
    
    # start training
    for epoch in range(start_epoch, epochs):
        train(log_interval, resnet_vae, device, train_loader, optimizer, epoch)
        if 0:
            validation(resnet_vae, device, optimizer, valid_loader, epoch)

    return 0


if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == 'resume':
        main(resume=True)
    else:
        main(resume=False)
